from logging import DEBUG

from fastapi import APIRouter, Request, Response, status

from ..controllers.mlmodelname import get_mlmodelname_results
from ..dependencies.logger import initialize_logger
from ..pydantic_models.mlmodelname import RequestBodyModel, ResponseModel


logger = initialize_logger('mlmodelname', DEBUG)
router = APIRouter()


@router.post(
    "/api/v1/ml-models/ml-model-name",
    tags=["ML Model Name Location Model"],
    summary="Run the ML Model Name Location model",
    description="Run the ML Model Name Location model by providing a JSON input and expect a JSON output to be generated"
)
async def compute_mlmodelname(request: Request, response: Response, model_input: RequestBodyModel) -> ResponseModel:
    result = get_mlmodelname_results(logger, model_input.model_dump())

    response.status_code = result["status"]

    return {
        "message": result["message"],
        "method": request.method,
        "status": status.HTTP_200_OK,
        "url": request.url.path,
        "output": result["output"]
    }
