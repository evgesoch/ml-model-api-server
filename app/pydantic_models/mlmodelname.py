from pydantic import BaseModel, model_validator


class RequestBodyModel(BaseModel):
    attr1: str | None = None
    attr2: str
    attr3: int

    @model_validator(mode='after')
    def validate_attr1(self) -> 'RequestBodyModel':
        attr1 = self.attr1
        attr2 = self.attr2

        if attr1 == attr2:
            raise ValueError("attr1 can't be equal to attr2")

        return self


class MlModelNameOutputModel(BaseModel):
    attr4: str
    attr5: str
    attr6: int

    @model_validator(mode='after')
    def validate_attr1(self) -> 'MlModelNameOutputModel':
        attr4 = self.attr4
        attr5 = self.attr5

        if attr4 == attr5:
            raise ValueError("attr4 can't be equal to attr5")

        return self


class ResponseModel(BaseModel):
    message: str | None = None
    method: str
    status: int
    url: str
    output: MlModelNameOutputModel
