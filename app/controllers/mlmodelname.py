from logging import Logger

from fastapi import HTTPException, status
from pydantic import ValidationError

from ..pydantic_models.mlmodelname import MlModelNameOutputModel
from ..ml_models.mlmodelname.model import run_model


def get_mlmodelname_results(logger: Logger, model_input: dict) -> dict:
    """Get the ml model name model results

    Args:
        logger (Logger): The logger object
        model_input (dict): The model input dictionary

    Returns:
        dict: The model output dictionary
    """

    result = {}

    # Run the model
    model_output = run_model(logger, model_input)

    # Validate the model output
    try:
        model_output = MlModelNameOutputModel(**model_output)
    except ValidationError as e:
        logger.error(e)
        message = "Model output validation failed"
        logger.error(message)
        # Not really good
        # result.update({
        #     "message": message,
        #     "status": status.HTTP_400_BAD_REQUEST,
        #     "output": None
        # })

        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=e.errors())

    result.update({
        "message": "Model ran sucessfully",
        "status": status.HTTP_200_OK,
        "output": model_output
    })

    return result
