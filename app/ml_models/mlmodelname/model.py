"""The ML Model Name model
"""

from logging import Logger
from time import time


def run_model(logger: Logger, model_input: dict) -> dict:
    """Run the ML Model Name model

    Args:
        logger (Logger): The logger object
        model_input (dict): A dictionary with the model input

    Returns:
        dict: A dictionary with the model output
    """

    start_time = time()

    #
    # Model algorithm here
    #

    model_output = {
        "attr4": model_input["attr1"],
        "attr5": model_input["attr2"],
        "attr6": model_input["attr3"]
    }

    total_time = round(time() - start_time, 6)
    logger.info("Total runtime: %s seconds", total_time)

    return model_output
