"""The logger initialization module
"""

from logging import Formatter, getLogger, Logger, StreamHandler
from logging.handlers import RotatingFileHandler
from os.path import join


LOG_FILE_MAX_BYTES = 50e6
LOG_MSG_FMT = "%(asctime)s %(levelname)-8s %(name)s \
%(filename)s#L%(lineno)d %(message)s"
LOG_DT_FMT = "%Y-%m-%d %H:%M:%S"


def initialize_logger(name: str, log_level: int) -> Logger:
    """Initialize the logger

    Args:
        name (str): The logger name
        log_level (int): The log level

    Returns:
        Logger: A logger object
    """

    logger = getLogger(name)
    formatter = Formatter(fmt=LOG_MSG_FMT, datefmt=LOG_DT_FMT)
    stream_handler = StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)
    file_handler = RotatingFileHandler(
        join("/src/app/logs/", "logs.txt"),
        mode='w',
        backupCount=1,
        maxBytes=LOG_FILE_MAX_BYTES
    )
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.setLevel(log_level)

    return logger
