from fastapi import FastAPI, Request

from .routers import mlmodelname


app = FastAPI()


app.include_router(mlmodelname.router)


@app.get("/", tags=["Root route"], summary="Root route just for testing purposes")
async def read_root(request: Request):
    return {
        "Hello": "World",
        "req": request.url
    }
