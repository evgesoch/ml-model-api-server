# ML Model API Server

## Description

A machine learning model API server that provides the model made with [FastAPI](https://fastapi.tiangolo.com/).

## Running the model

### Build the container image

The model can be packaged into a container through the `Dockerfile` definition and the docker image can be built using the following command:

```shell
docker build -t ml-model-api-server .
```

### Create the container and run the model

Having prepared the image in the build step, the model can be run inside the container. Data can be passed inside the container with volume mounts.

```shell
# For dev with source code as volume
docker run -v $PWD/app:/src/app --name ml-model-api-server -p 81:81 ml-model-api-server

# With -d to detach
docker run -v $PWD/app:/src/app -d --name ml-model-api-server -p 81:81 ml-model-api-server

# Without volume like it would run on prod
docker run -d --name ml-model-api-server -p 81:81 ml-model-api-server
```

Now the application runs on `http://localhost:81`. You can access the Swagger docs on `http://localhost:81/docs`.
