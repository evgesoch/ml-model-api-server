FROM python:3.10.9-slim-bullseye

WORKDIR /src

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    liblapack-dev libatlas-base-dev && \
    rm -rf /var/lib/apt/lists/*

COPY requirements.txt README.md /src/

EXPOSE 81

RUN pip install --upgrade pip && \
    pip install -r /src/requirements.txt

COPY app /src/app

CMD ["uvicorn", "app.main:app", "--reload", "--host", "0.0.0.0", "--port", "81"]
